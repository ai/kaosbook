
epub:
	(cd epub-it && \
	 zip -X0 ../kaosbook-it.epub mimetype && \
	 zip -rDX9 ../kaosbook-it.epub * -x "*.DS_Store" -x mimetype)

clean:
	-rm kaosbook-it.epub
